import React, { Component } from "react";
import ReactDOM from "react-dom";
import { connect } from "react-redux";
import { addReminder, deleteReminder, clearReminders } from "../actions";
import moment from "moment";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      text: "",
      dueDate: ""
    };
  }
  addReminder() {
    var date = moment(new Date(this.state.dueDate));
    var isFuture = date.isAfter(moment());
    if (isFuture && date.isValid) {
      this.props.addReminder(this.state.text, this.state.dueDate);
      ReactDOM.render(
        <div className="alert alert-success" role="alert">
          <button
            type="button"
            className="close"
            data-dismiss="alert"
            aria-label="Close"
            onClick={() =>
              ReactDOM.unmountComponentAtNode(
                document.getElementById("alert-window")
              )
            }
          >
            <span aria-hidden="true">&times;</span>
          </button>
          <strong>Success!</strong>New reminder added!
        </div>,
        document.getElementById("alert-window")
      );
    } else {
      ReactDOM.render(
        <div className="alert alert-danger" role="alert" id="alertWindow">
          <button
            type="button"
            className="close"
            data-dismiss="alert"
            aria-label="Close"
            onClick={() =>
              ReactDOM.unmountComponentAtNode(
                document.getElementById("alert-window")
              )
            }
          >
            <span aria-hidden="true">&times;</span>
          </button>
          <strong>Error!</strong>The date inserted is incorrect or is not a
          future date!
        </div>,
        document.getElementById("alert-window")
      );
      return -1;
    }
  }
  deleteReminder(id) {
    this.props.deleteReminder(id);
  }
  clearReminders() {
    this.props.clearReminders();
  }
  renderReminders() {
    const { reminders } = this.props;
    return (
      <ul className="list-group">
        {reminders.map(reminder => {
          return (
            <li key={reminder.id} className="list-group-item">
              <div className="list-item">
                <div>{reminder.text}</div>
                <div>
                  <em>{moment(new Date(reminder.dueDate)).fromNow()}</em>
                </div>
              </div>
              <div
                className="list-item delete-button"
                onClick={() => this.deleteReminder(reminder.id)}
              >
                &#x2715;
              </div>
            </li>
          );
        })}
      </ul>
    );
  }
  render() {
    return (
      <div className="App">
        <div className="title">ReminderPRO</div>
        <div className="form-inline reminder-form">
          <div className="form-group">
            <input
              className="form-control"
              placeholder="I have too..."
              onChange={event => this.setState({ text: event.target.value })}
            />
            <input
              className="form-control"
              type="datetime-local"
              onChange={event => this.setState({ dueDate: event.target.value })}
            />
          </div>
          <div id="alert-window" />
          <button
            type="button"
            className="btn btn-success"
            onClick={() => this.addReminder()}
          >
            Add Reminder!
          </button>
          {this.renderReminders()}
          <button
            className="btn btn-danger"
            onClick={() => this.props.clearReminders()}
          >
            Clear Reminders!
          </button>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    reminders: state
  };
}

export default connect(mapStateToProps, {
  addReminder,
  deleteReminder,
  clearReminders
})(App);
